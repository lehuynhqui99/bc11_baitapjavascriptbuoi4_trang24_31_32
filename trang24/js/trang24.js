const xepLoaiBtn = document.getElementById('xepLoaiBtn');
const tenSinhVien = document.getElementById('tenSinhVien');
const toan = document.getElementById('toan');
const ly = document.getElementById('ly');
const hoa = document.getElementById('hoa');
const showKetQua = document.getElementById('showKetQua');

const text1 = document.getElementById('text1')
const text2 = document.getElementById('text2')
const text3 = document.getElementById('text3')

let trungBinh





// validation form
const formControl1 = document.querySelectorAll('.form-control-1');
const errorMessage = document.querySelectorAll('.error-message');
var message = ''
formControl1.forEach((formControl, index) => {
    formControl.addEventListener('keyup', e => {
        let Numberpattern = /[0-9]+/
        let TextPattern = /[A-Za-z]+/

        if (!Numberpattern.test(formControl.value) || TextPattern.test(formControl.value) || formControl.value > 10 || formControl.value <= 0) {
            message = 'Giá trị nhập vào không hợp lệ'
            errorMessage[index].innerHTML = message
            if (message) {
                xepLoaiBtn.disabled = true
            }
        } else {
            message = ''
            errorMessage[index].innerHTML = message
            if (toan.value !== '' && ly.value !== '' && hoa.value !== '' && text1.innerHTML === '' && text2.innerHTML === '' && text3.innerHTML === '') {
                xepLoaiBtn.disabled = false
            }

        }
    })
})


//xử lý sự kiện onlick (onsubmit)
xepLoaiBtn.addEventListener('click', e => {
    trungBinh = Number(((Number(toan.value) + Number(ly.value) + Number(hoa.value)) / 3).toFixed(1))
    var xepLoai

    switch (true) {
        case (trungBinh >= 8.5):
            xepLoai = 'Học sinh giỏi'
            break;

        case (trungBinh >= 6.5 && trungBinh < 8.5):
            xepLoai = 'Học sinh khá'
            break;

        case (trungBinh >= 5 && trungBinh < 6.5):
            xepLoai = 'Học sinh trung bình'
            break;
        default:
            xepLoai = 'Học sinh yếu'
            break;
    }

    showKetQua.innerHTML = `
        <tt>
            <h4>BẢNG ĐIỂM</h4>
            <small class="text-muted">Dành cho sinh viên</small>
            <div class="text-left mt-5">
                <h6><b>Họ và tên sinh viên: ${tenSinhVien.value}</b></h6>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Môn</th>
                            <th>Điểm</th>
                            <th>Ghi chú</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Toán</td>
                            <td>${toan.value}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Lý</td>
                            <td>${ly.value}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Hoá</td>
                            <td>${hoa.value}</td>
                            <td></td>
                        </tr>

                    </tbody>
                    <tfoot>
                        <tr>
                            <td class="text-center font-weight-bolder" colspan="2">ĐIỂM TRUNG BÌNH</td>
                            <td class="font-weight-bolder">${trungBinh}</td>
                        </tr>
                    </tfoot>
                </table>
                <h6 class="alert-success p-2"><b>Xếp loại: ${xepLoai}</b></h6>
            </div>
        </tt>
    `
    e.preventDefault()

})

