const chanLeBtn = document.getElementById('chanLeBtn')
const chanLe1 = document.getElementById('chanLe1')
const chanLe2 = document.getElementById('chanLe2')
const chanLe3 = document.getElementById('chanLe3')
const showChanLe = document.getElementById('showChanLe')


chanLeBtn.addEventListener('click', e => {
    e.preventDefault();


    // validation 
    if (chanLe1.value === '' || chanLe2.value === '' || chanLe3.value === '' 
        || chanLe1.value.includes(".") || chanLe2.value.includes(".") || chanLe3.value.includes(".")
        || chanLe1.value.includes(",") || chanLe2.value.includes(",") || chanLe3.value.includes(",")) {
        alert("Tất cả các trường không được bỏ trống, và phải là số nguyên.")
        showChanLe.innerHTML = ``

    } else {

        let chan = 0;
        let le = 0;
    
        Math.abs(chanLe1.value) % 2 === 0 ? chan++ : le++
        Math.abs(chanLe2.value) % 2 === 0 ? chan++ : le++
        Math.abs(chanLe3.value) % 2 === 0 ? chan++ : le++
    
    
        showChanLe.innerHTML = `Số chẳn là: ${chan}. Số lẻ là: ${le}`
    }

})