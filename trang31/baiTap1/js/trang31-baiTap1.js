const sapXepBtn = document.getElementById('sapXep')
const soNguyen1 = document.getElementById('soNguyen1')
const soNguyen2 = document.getElementById('soNguyen2')
const soNguyen3 = document.getElementById('soNguyen3')
const showKetQuaSapXep = document.getElementById('showKetQuaSapXep')


sapXepBtn.addEventListener('click', e => {
    let a = Number(soNguyen1.value)
    let b = Number(soNguyen2.value)
    let c = Number(soNguyen3.value)
    
    e.preventDefault();


    // validation 
    if (soNguyen1.value === '' || soNguyen2.value === '' || soNguyen3.value === '' 
        || soNguyen1.value.includes(".") || soNguyen2.value.includes(".") || soNguyen3.value.includes(".")
        || soNguyen1.value.includes(",") || soNguyen2.value.includes(",") || soNguyen3.value.includes(",")) {
        alert("Tất cả các trường không được bỏ trống, và phải là số nguyên.")
        showKetQuaSapXep.innerHTML = ``

    } else {

        if ( a >= b && a>= c && b >= c) {
            showKetQuaSapXep.innerHTML =  `${soNguyen3.value}, ${soNguyen2.value}, ${soNguyen1.value}`;
        }

        if ( b >= a && b>= c && a >= c) {
            showKetQuaSapXep.innerHTML =  `${soNguyen3.value}, ${soNguyen1.value}, ${soNguyen2.value}`;
        }
    
        if ( a >= b && a>= c && b <= c) {
            showKetQuaSapXep.innerHTML =  `${soNguyen2.value},  ${soNguyen3.value}, ${soNguyen1.value}`;
        } 
    
        if ( c >= a && c>= b && b <= a) {
            showKetQuaSapXep.innerHTML =  `${soNguyen2.value},  ${soNguyen1.value}, ${soNguyen3.value}`;
        } 

        if ( a <= b && a <= c && b >= c) {
            showKetQuaSapXep.innerHTML =  `${soNguyen1.value},  ${soNguyen3.value}, ${soNguyen2.value}`;
    
        } 
        
        if ( a <= b && a <= c && b <= c) {
            showKetQuaSapXep.innerHTML =  `${soNguyen1.value},  ${soNguyen2.value}, ${soNguyen3.value}`;
    
        }
    
        // console.log(soNguyen1.value, soNguyen2.value, soNguyen3.value)
    }


    

})