const chaoHoi = document.getElementById('chaoHoi')
const chaoHoiBtn = document.getElementById('chaoHoiBtn')
const chaoHoiMessage = document.getElementById('chaoHoiMessage')

chaoHoiBtn.addEventListener('click', e => {
    e.preventDefault()
    switch (chaoHoi.value) {
        case 'M': {
            chaoHoiMessage.innerHTML = 'Xin chào mẹ!'
            break;
        }
        case 'A': {
            chaoHoiMessage.innerHTML = 'Xin chào anh trai!'
            break;
        }
        
        case 'E': {
            chaoHoiMessage.innerHTML = 'Xin chào em gái!'
            break;
        }
    
        default:
            chaoHoiMessage.innerHTML = 'Xin chào bố!'
            break;
    }
})